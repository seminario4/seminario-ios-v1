//
//  UserController.swift
//  SeminariosUTEC
//
//  Created by Jhosep Islam on 5/12/21.
//

import Foundation
import UIKit


class UserController {
    let networkController = NetworkController()

    func doLogin(User: String, Password: String, Contex: ViewController)  {
        let request = self.networkController.doPost(urlString: "http://192.168.1.36:45455/api/users/login", data: ["Nombre_usuario" : User, "Password": Password])
            
        let task =  URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                let response = response as? HTTPURLResponse,
                error == nil else {                                              // check for fundamental networking error
                print("error", error ?? "Unknown error")
                return
            }

            guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }
            do {
                let result = try JSONDecoder().decode(UserModel.self, from: data)

                let privilegio = result.id_privilegio
                print(privilegio)
            } catch {
                print(error)
            }
        }
        task.resume()

    }
}
