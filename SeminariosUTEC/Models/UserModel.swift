//
//  UserModel.swift
//  SeminariosUTEC
//
//  Created by Jhosep Islam on 5/12/21.
//

import Foundation

class UserModel: Decodable {
    
    let nombre_usuario : String
    let password: String
    let id_privilegio: Int
    
    
}
