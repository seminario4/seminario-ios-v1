//
//  ViewController.swift
//  SeminariosUTEC
//
//  Created by Jhosep Islam on 5/12/21.
//

import UIKit
class ViewController: UIViewController {
    @IBOutlet weak var txtUser: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var lbUserError: UILabel!
    @IBOutlet weak var lbPasswordErro: UILabel!
    
    let userController = UserController();
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        lbUserError.isHidden =  true
        lbPasswordErro.isHidden = true
    }

    @IBAction func doLogin(_ sender: UIButton) {
        if  (txtUser.text!.isEmpty)  {
            self.lbUserError.text = "Usuario es requerido"
            self.lbUserError.isHidden = false
        } else if (txtPassword.text!.isEmpty) {
            self.lbPasswordErro.text = "Contraseña es requerida"
            self.lbUserError.isHidden = true
            self.lbPasswordErro.isHidden = false
        } else {
            self.lbPasswordErro.isHidden = true
            self.lbUserError.isHidden = true
            
            userController.doLogin(User: "2504642017", Password: "123", Contex: self)
             
            /*
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let homeViewController = storyboard?.instantiateViewController(withIdentifier: "home") as! HomeViewController
            
            self.present(homeViewController, animated: true, completion: nil)*/

        }
    }
    
}

