//
//  NetworkController.swift
//  SeminariosUTEC
//
//  Created by Jhosep Islam on 5/12/21.
//

import Foundation


class NetworkController {
    
    
    
    
    func doPost(urlString: String, data: [String : Any]) -> URLRequest  {
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        
        let parameters = try? JSONSerialization.data(withJSONObject: data, options: [])
        
        request.httpBody = parameters
        
        return request
    }
}
